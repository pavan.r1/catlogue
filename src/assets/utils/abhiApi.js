const moveToHotLead = "https://app.backend.taskmo.co/hotlead/moveToHotLead";
const asmCardData = "https://spapp.backend.taskmo.co/agreement/overview";
const getcurrentDate = "https://farming-backend.taskmo.in/test/getcurrenttime";
const sendWhatsappForSp =
  "https://app.backend.taskmo.co/whatsapp/sendWhatsappForSp";

export { moveToHotLead, sendWhatsappForSp, asmCardData, getcurrentDate };
