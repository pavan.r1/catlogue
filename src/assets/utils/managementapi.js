const daily_tracking = "/daily_tracking";
const pipeline_tracking = "/pipeline_tracking";
const business_team_rev = "/business_team_rev";
const filter_fy = "/filter_fy";
const projection = "/supplytoken/projection";
const investors_daily_target = "/investors_daily_target";
const investors_daily_approve_target = "/investors_daily_approve_target";
const investors_details = "/investors_details";
const dailycollectable = "/invoatoken/dailycollectable";
const pipeline_card = "/pipeline_card";
const projectionofweek = "/supplytoken/projectionofweek";

const daily_approve_tracking = "/daily_approve_tracking";
const bussiness_stats_overall = "/bussiness_stats_overall";
const annual_supply = "/annual_supply";

export {
  daily_tracking,
  pipeline_tracking,
  business_team_rev,
  pipeline_card,
  filter_fy,
  projection,
  investors_daily_target,
  investors_details,
  dailycollectable,
  daily_approve_tracking,
  investors_daily_approve_target,
  projectionofweek,
  bussiness_stats_overall,
  annual_supply,
};
