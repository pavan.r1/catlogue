import React from "react";
import { Col, Container, Row } from "reactstrap";
import BreadCrumb from "../../components/common/BreadCrumb";
// import MainProduct from "./MainProduct";
import SubProduct from "./SubProduct";

const CatlougeMain = () => {
  return (
    <div className="page-content">
      <Container fluid>
        {/* <BreadCrumb title="Products" pageTitle="Ecommerce" /> */}
        <Row>
          {/* <Col xs="3" style={{ padding: "0px", paddingRight: "12px" }}>
            <MainProduct />
          </Col> */}
          <Col xs="12">
            <SubProduct />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default CatlougeMain;
