import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Row,
  UncontrolledCollapse,
} from "reactstrap";
import { Link } from "react-router-dom";
import classnames from "classnames";
import "./mainproduct.css";

const MainProduct = () => {
  const [cate, setCate] = useState("all");
  return (
    <div>
      <Row>
        <Col>
          <Card>
            <CardHeader
              style={{
                position: "sticky",
                top: "0px",
                zIndex: "99",
                background: "#fefefe;",
              }}
            >
              <p className="text-muted  fs-12 fw-medium m-0 ">
                <h5>Category List</h5>
              </p>
            </CardHeader>
            <CardBody
              className="cat-list"
              style={{ height: "80vh", overflow: "scroll" }}
            >
              <div className="accordion accordion-flush ">
                <div>
                  <div>
                    <ul className="list-unstyled mb-0 filter-list">
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Kitchen Storage & Containers"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          // onClick={() =>
                          //   categories("Kitchen Storage & Containers")
                          // }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Grocery</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">5</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Clothes"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          // onClick={() => categories("Clothes")}
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Fashion</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">5</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Watches"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          // onClick={() => categories("Watches")}
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Watches</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "electronics"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          // onClick={() => categories("electronics")}
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Electronics</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">5</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Furniture"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          // onClick={() => categories("Furniture")}
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Furniture</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">6</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Furniture"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          // onClick={() => categories("Furniture")}
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">
                              Automative Accessories
                            </h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">6</span>
                          </div>
                        </Link>
                      </li>

                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "appliances"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() => categories("appliances")}
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Appliances</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                      <li className="mb-2">
                        <Link
                          to="#"
                          className={
                            cate === "Bags, Wallets and Luggage"
                              ? "active d-flex py-1 align-items-center"
                              : "d-flex py-1 align-items-center"
                          }
                          //   onClick={() =>
                          //     categories("Bags, Wallets and Luggage")
                          //   }
                        >
                          <div className="flex-grow-1">
                            <h5 className="fs-13 mb-0 listname">Kids</h5>
                          </div>
                          <div className="flex-shrink-0 ms-2">
                            <span className="badge bg-light text-muted">7</span>
                          </div>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
      {/* </Container> */}
    </div>
  );
};

export default MainProduct;
