import React, { useRef, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import {
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  UncontrolledDropdown,
} from "reactstrap";
import classnames from "classnames";
import TableContainer from "./TableContainer";
import "./subproduct.css";
import AddModal from "./catlouge modals/AddModal";
import "./subproduct.css";
import PriceAppTable from "./PriceAppTable";
import ProductAppTable from "./catlouge modals/ProductAppTable";
import avatar from "../../assets/images/users/avatar-1.jpg";
import ManualUpload from "./catlouge modals/ManualUpload";
import All from "./viewproduct/Tables/All";

const SubProduct = () => {
  const [activeTab, setActiveTab] = useState("1");
  const [productList, setProductList] = useState([]);
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [open3, setOpen3] = useState(false);
  const scrollContainerRef = useRef(null);
  const history = useHistory();

  const products = [1, 2, 3];

  const toggleTab = (tab, type) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
      let filteredProducts = products;
      if (type !== "all") {
        filteredProducts = products.filter(
          (product) => product.status === type
        );
      }
      setProductList(filteredProducts);
    }
  };

  //

  const handleScrollLeft = () => {
    if (scrollContainerRef.current) {
      scrollContainerRef.current.scrollTo({
        left: scrollContainerRef.current.scrollLeft - 100,
        behavior: "smooth", // Use 'auto' for instant scrolling
      });
    }
  };

  const handleScrollRight = () => {
    if (scrollContainerRef.current) {
      scrollContainerRef.current.scrollTo({
        left: scrollContainerRef.current.scrollLeft + 100,
        behavior: "smooth", // Use 'auto' for instant scrolling
      });
    }
  };

  return (
    <div>
      {/* <Container fluid> */}
      <Row>
        <Col>
          <div>
            <div>
              <div className="card">
                <div className="card-header border-0">
                  <div className="row g-4">
                    <div className="col-sm-auto">
                      <div>
                        <div>
                          <h5 className="mb-3">Sub Category List</h5>
                        </div>

                        <div
                          className="d-flex gap-2 align-items-center overflow-scroll my-scroll"
                          ref={scrollContainerRef}
                          style={{ whiteSpace: "nowrap" }}
                        >
                          <div
                            style={{
                              position: "sticky",
                              top: "0px",
                              left: "0px",
                              padding: "8px",
                              background: "#fff",
                            }}
                            // onClick={handleScrollLeft}
                          >
                            <span
                              className="cursor-pointer"
                              style={{
                                position: "sticky",
                                top: "0px",
                                left: "0px",
                              }}
                              onClick={handleScrollLeft}
                            >
                              <i
                                className=" ri-arrow-left-s-line fs-21 bg-light text-primary py-1 px-1"
                                style={{ borderRadius: "8px" }}
                              ></i>
                            </span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Fruits</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Vegetables</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Dairy</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Meat</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Snacks</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Bread & Bakery</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Beverages</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Baking</span>
                          </div>
                          <div className="sub_product cursor-pointer d-flex gap-2 align-items-center">
                            <img
                              src={avatar}
                              alt=""
                              className="rounded-circle avatar-xxs"
                            ></img>
                            <span>Rice & Cereals </span>
                          </div>
                          <div
                            style={{
                              position: "sticky",
                              top: "0px",
                              right: "0px",
                              left: "0px",
                              padding: "8px",
                              background: "#fff",
                            }}
                          >
                            <span
                              className="cursor-pointer"
                              style={{
                                position: "sticky",
                                top: "0px",
                                right: "0px",
                                left: "0px",
                              }}
                              onClick={handleScrollRight}
                            >
                              <i
                                className=" ri-arrow-right-s-line fs-21 bg-light px-1 py-1 text-primary"
                                style={{ borderRadius: "8px" }}
                              ></i>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* <div className="col-sm">
                        <div className="d-flex justify-content-sm-end">
                          <div className="search-box ms-2">
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Search Products..."
                            />
                            <i className="ri-search-line search-icon"></i>
                          </div>
                        </div>
                      </div> */}
                  </div>
                </div>

                <div>
                  <div>
                    <hr style={{ width: "100%", color: "#e0e0e0" }} />
                  </div>

                  <div
                    style={{
                      marginRight: "226px",
                      display: "flex",
                      justifyContent: "flex-end",
                    }}
                  >
                    {/* <Link
                      className="btn btn-success"
                      onClick={() => {
                        setOpen2(!open2);
                        history.push("/add-product");
                      }}
                    >
                      <i className=" ri-add-line align-bottom me-1"></i>
                      Add Product
                    </Link> */}
                    {/* <UncontrolledDropdown
                      className="dropdown d-inline-block mt-2"
                      
                    >
                      <DropdownToggle
                        style={{ background: "#fff", border: "none" }}
                      >
                        <Link
                         
                          className="btn btn-success"
                        >
                          <i className=" ri-add-line align-bottom me-1"></i>
                          Add Product
                        </Link>
                      </DropdownToggle>
                      <DropdownMenu
                        className="dropdown-menu-end"
                        style={{ minWidth: "152px" }}
                      >
                        <DropdownItem
                          className="edit-item-btn d-flex align-items-center"
                          onClick={() => setOpen2(!open2)}
                        >
                          Manual Add
                        </DropdownItem>

                        <DropdownItem
                          className="edit-item-btn d-flex align-items-center"
                          onClick={() => setOpen(!open)}
                        >
                          Bulk Upload
                        </DropdownItem>
                       
                      </DropdownMenu>
                    </UncontrolledDropdown> */}
                  </div>
                </div>

                <div className="card-header ">
                  <div className="row align-items-center">
                    <div className="col">
                      <Nav
                        className="nav-tabs-custom card-header-tabs border-bottom-0"
                        role="tablist"
                      >
                        <NavItem>
                          <NavLink
                            className={classnames(
                              { active: activeTab === "1" },
                              "fw-semibold"
                            )}
                            onClick={() => {
                              toggleTab("1", "all");
                            }}
                            href="#"
                          >
                            All
                            <span className="badge badge-soft-danger align-middle rounded-pill ms-1">
                              12
                            </span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames(
                              { active: activeTab === "2" },
                              "fw-semibold"
                            )}
                            onClick={() => {
                              toggleTab("2", "published");
                            }}
                            href="#"
                          >
                            QC in Progress
                            <span className="badge badge-soft-danger align-middle rounded-pill ms-1">
                              5
                            </span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames(
                              { active: activeTab === "3" },
                              "fw-semibold"
                            )}
                            onClick={() => {
                              toggleTab("3", "published");
                            }}
                            href="#"
                          >
                            QC Error
                            <span className="badge badge-soft-danger align-middle rounded-pill ms-1">
                              5
                            </span>
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames(
                              { active: activeTab === "4" },
                              "fw-semibold"
                            )}
                            onClick={() => {
                              toggleTab("4", "draft");
                            }}
                            href="#"
                          >
                            QC Pass
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames(
                              { active: activeTab === "5" },
                              "fw-semibold"
                            )}
                            onClick={() => {
                              toggleTab("5", "draft");
                            }}
                            href="#"
                          >
                            Draft
                          </NavLink>
                        </NavItem>
                      </Nav>
                    </div>
                    <div className="col-auto">
                      <div id="selection-element">
                        <div className="my-n1 d-flex align-items-center text-muted">
                          Select{" "}
                          <div
                            id="select-content"
                            className="text-body fw-semibold px-1"
                          ></div>{" "}
                          Result{" "}
                          <button
                            type="button"
                            className="btn btn-link link-danger p-0 ms-3"
                          >
                            Remove
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="card-body" style={{ height: "57vh" }}>
                  <TabContent className="text-muted">
                    <TabPane>
                      <div
                        id="table-product-list-all"
                        className="table-card gridjs-border-none pb-2"
                      >
                        {activeTab === "1" ? (
                          // <TableContainer />
                          <div>
                            <div
                              className="d-flex align-items-center justify-content-between"
                              style={{
                                padding: "12px",
                                paddingBottom: "0px",
                                marginTop: "12px",
                              }}
                            >
                              <div className="d-flex align-items-center">
                                <p
                                  className="m-0 fs-15 text-dark"
                                  style={{ width: "80px" }}
                                >
                                  Filter by:
                                </p>
                                <select
                                  className="form-select "
                                  aria-label="Default select example"
                                  style={{ width: "150px" }}
                                >
                                  <option selected>select category</option>
                                  <option value="1">One</option>
                                  <option value="2">Two</option>
                                  <option value="3">Three</option>
                                </select>
                              </div>

                              <div style={{ marginRight: "228px" }}>
                                <button
                                  type="button"
                                  className="btn btn-success waves-effect waves-light"
                                  onClick={() =>
                                    history.push("/select-category")
                                  }
                                >
                                  <i className=" ri-add-line align-bottom me-1"></i>
                                  Add Product
                                </button>
                              </div>
                            </div>

                            <All />
                          </div>
                        ) : activeTab === "2" ? (
                          <PriceAppTable />
                        ) : activeTab === "3" ? (
                          <ProductAppTable />
                        ) : null}
                      </div>
                    </TabPane>
                  </TabContent>
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>
      {/* </Container> */}
      <AddModal setOpen={setOpen} open={open} />
      <ManualUpload open={open2} setOpen={setOpen2} />
    </div>
  );
};

export default SubProduct;
