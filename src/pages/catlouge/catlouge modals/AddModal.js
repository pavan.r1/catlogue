import React from "react";
import { useState } from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import ManualUpload from "./ManualUpload";

const AddModal = ({ open, setOpen }) => {
  const [openManMod, setOpenManMod] = useState(false);
  const hiddenFile = React.useRef(null);

  const handleFileChange = (e) => {
    const fileData = new FormData();
    fileData.append("file", e.target.files[0]);
  };

  const handleFileClick = () => {
    hiddenFile.current.click();
  };
  return (
    <div>
      <Modal
        id="signupModals"
        tabIndex="-1"
        isOpen={open}
        toggle={() => setOpen(false)}
        centered={true}
        size="md"
      >
        <ModalHeader
          className="p-3"
          toggle={() => setOpen(false)}
        ></ModalHeader>

        <ModalBody>
          <div className="text-center ">
            <div className="d-flex gap-4 justify-content-center">
              <span
                className="badge badge-soft-primary cursor-pointer"
                style={{ padding: "20px 20px", fontSize: "16px" }}
                // onClick={() => setOpenManMod(!openManMod)}
              >
                <i className=" ri-download-cloud-2-line align-middle me-2 fs-18"></i>
                Download Sample
              </span>
              <span
                className="badge badge-soft-primary cursor-pointer"
                style={{
                  padding: "20px 20px ",
                  fontSize: "16px",
                  minWidth: "220px",
                }}
                onClick={() => handleFileClick()}
              >
                <i className=" ri-upload-cloud-2-line align-middle me-2 fs-18"></i>
                Bulk Upload
              </span>
              <input
                type="file"
                ref={hiddenFile}
                style={{ display: "none" }}
                onChange={handleFileChange}
              />
            </div>
          </div>
        </ModalBody>
      </Modal>
      <ManualUpload open={openManMod} setOpen={setOpenManMod} />
    </div>
  );
};

export default AddModal;
