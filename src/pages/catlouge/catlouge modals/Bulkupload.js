import React, { useState } from "react";
import Dropzone from "react-dropzone";

const Bulkupload = () => {
  const [selectedFiles, setselectedFiles] = useState([]);
  function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return "0 Bytes";
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  function handleAcceptedFiles(files) {
    files.map((file) =>
      Object.assign(file, {
        preview: URL.createObjectURL(file),
        formattedSize: formatBytes(file.size),
      })
    );
    setselectedFiles(files);
    console.log(files[0], "files");
  }
  return (
    <div>
      <Dropzone
        onDrop={(acceptedFiles) => {
          handleAcceptedFiles(acceptedFiles);
        }}
      >
        {({ getRootProps, getInputProps }) => (
          <div className="dropzone dz-clickable">
            <div className="dz-message needsclick" {...getRootProps()}>
              <div className="mb-3">
                <i className="display-4 text-success ri-upload-cloud-2-fill " />
              </div>
              <h4 className="mb-1 text-success">
                Select a CSV file to upload.
              </h4>
              <p className="fs-16 text-muted">or drag and drop it here </p>
            </div>
          </div>
        )}
      </Dropzone>
      <div
        className="mt-1 d-flex justify-content-end "
        style={{ fontWeight: "500" }}
      >
        <div className="cursor-pointer">
          <i className=" ri-download-2-line me-1"></i>
          <span>Download sample csv</span>
        </div>
      </div>
    </div>
  );
};

export default Bulkupload;
