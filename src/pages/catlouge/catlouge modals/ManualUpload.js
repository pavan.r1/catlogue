import React, { useState } from "react";
import {
  Col,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
} from "reactstrap";
import Flatpickr from "react-flatpickr";
import { api } from "../../../globalConfig";
import { upload_issue_proof } from "../../../assets/utils/catlougeapis";
import axios from "axios";
import Bulkupload from "./Bulkupload";

const ManualUpload = ({ open, setOpen }) => {
  const [imgs, setImgs] = useState([]);
  //   const [img, setImg] = useState();
  const [selectedOption, setSelectedOption] = useState("manualupload");

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
    console.log(event.target.value);
  };

  console.log(imgs, "images");

  const handleImageChange = (e) => {
    const link = api.AWS_URL + upload_issue_proof;
    const axiosData = new FormData();
    axiosData.append("file", e.target.files[0]);

    console.log(e.target.files[0], "file");
    axios
      .post(link, axiosData)
      .then((res) => {
        // setImg(res.data.url);
        console.log(res.data);

        // setImgs([...imgs, res.data.url]);
      })

      .catch((e) => {
        console.log(e);
      });
    setImgs([...imgs, e.target.files[0]]);
  };
  return (
    <div>
      <Modal
        id="signupModals"
        tabIndex="-1"
        isOpen={open}
        toggle={() => setOpen(false)}
        centered={true}
        size="lg"
      >
        <ModalHeader className="p-3" toggle={() => setOpen(false)}>
          Add Details
        </ModalHeader>

        <ModalBody>
          <div>
            <Row>
              <Col xs="12">
                <div className="d-flex gap-5 mb-4">
                  <div className="d-flex gap-3 align-items-center">
                    <input
                      type="radio"
                      value="manualupload"
                      checked={selectedOption === "manualupload"}
                      style={{ transform: "scale(1.3)" }}
                      onChange={handleOptionChange}
                    />
                    <label style={{ margin: "0px", fontSize: "14px" }}>
                      One by one
                    </label>
                  </div>
                  <div className="d-flex gap-3 align-items-center">
                    <input
                      type="radio"
                      value="bulkupload"
                      checked={selectedOption === "bulkupload"}
                      style={{ transform: "scale(1.3)" }}
                      onChange={handleOptionChange}
                    />
                    <label style={{ margin: "0px", fontSize: "14px" }}>
                      Bulk Upload
                    </label>
                  </div>
                </div>
              </Col>
            </Row>
            {selectedOption === "manualupload" ? (
              <>
                <Row>
                  <Col xs="12">
                    <div className="d-flex gap-4 align-items-center">
                      <div>
                        <label htmlFor="formFile" className="form-label">
                          Upload Images
                        </label>
                        <input
                          className="form-control"
                          type="file"
                          id="formFile"
                          onChange={(e) => handleImageChange(e)}
                        />
                      </div>

                      <div>
                        <img
                          src={"/user-dummy-img.jpg"}
                          alt="img"
                          width="50px"
                          // height="10px"
                          style={{ marginTop: "16px" }}
                        />
                      </div>
                    </div>
                  </Col>
                  <Col xs="6" className="mt-4">
                    <div>
                      <label htmlFor="basiInput" className="form-label">
                        Product Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="basiInput"
                      />
                    </div>
                  </Col>

                  <Col xs="6" className="mt-4">
                    <div>
                      <label htmlFor="basiInput" className="form-label">
                        MRP
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        id="basiInput"
                      />
                    </div>
                  </Col>
                  <Col xs="6" className="mt-4">
                    <div>
                      <Label className="form-label ">Self Life</Label>
                      <Flatpickr
                        className="form-control"
                        options={{
                          dateFormat: "d M, Y",
                        }}
                      />
                    </div>
                  </Col>
                  <Col xs="6" className="mt-4">
                    <div>
                      <label htmlFor="basiInput" className="form-label">
                        Country Of Origin
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        id="basiInput"
                      />
                    </div>
                  </Col>
                  <Col xs="6" className="mt-4">
                    <div>
                      <label htmlFor="basiInput" className="form-label">
                        Product Description
                      </label>
                      <textarea
                        className="form-control"
                        id="exampleFormControlTextarea5"
                        rows="3"
                      ></textarea>
                    </div>
                  </Col>
                </Row>
              </>
            ) : (
              <Bulkupload />
            )}
          </div>
        </ModalBody>
        <ModalFooter>
          <button
            type="button"
            className="btn btn-primary waves-effect waves-light"
          >
            Submit
          </button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ManualUpload;
