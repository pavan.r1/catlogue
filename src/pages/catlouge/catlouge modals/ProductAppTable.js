import React from "react";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap";
import DataTable from "react-data-table-component";
import DataTableExtensions from "react-data-table-component-extensions";
import "../pricetable.css";

const ProductAppTable = () => {
  const columns = [
    {
      name: "ID",
      width: "100px",
      omit: true,
      selector: (row) => row.qc_admin_id,
      sortable: true,

      // width:'180px',
      center: true,
      cell: (d) => <div>kml</div>,
    },

    {
      name: "Details",
      //   width: "250px",
      selector: (row) => row.full_name,
      sortable: true,

      //   width:'60px',
      // center: true,
      cell: (d) => (
        <div className="p-2">
          <div style={{ display: "flex", alignItems: "center", gap: "10px" }}>
            <div>
              {d?.brand_logo ? (
                <img
                  src={"/user-dummy-img.jpg"}
                  alt=""
                  className="rounded-circle avatar-sm"
                />
              ) : (
                <div className="avatar-sm">
                  <div className="avatar-title rounded-circle bg-soft-primary  text-primary">
                    {d?.brand_name?.charAt(0) ?? ""}
                  </div>
                </div>
              )}
            </div>

            <div>
              <div className="fs-12">product name testing</div>
              <div className="fs-10 text-muted">
                {d?.cmp_name ? d?.cmp_name : d?.companyname ?? ""}
              </div>
              <div
                style={{ display: "flex", gap: "5px", alignItems: "center" }}
              ></div>
            </div>
          </div>
        </div>
      ),
    },

    {
      name: "Category",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>Fruits</div>,
    },
    {
      name: "Amount",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>1234</div>,
    },
    {
      name: "View",
      selector: (row) => row.manager,
      sortable: true,
      alignItems: "center",
      center: true,
      cell: (d) => (
        <div>
          <i className=" ri-eye-fill fs-18 text-info"></i>
        </div>
      ),
    },
    {
      name: "Status",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => (
        <div>
          <span className="badge badge-soft-success">approved</span>
        </div>
      ),
    },

    {
      name: "Action",
      right: true,
      //   width: "70px",
      center: true,
      omit: true,
      //   type == "fin" && (role == "manager" || role == "head") ? false : true,
      cell: (d) => (
        <div>
          <UncontrolledDropdown
            className="dropdown d-inline-block"
            // onClick={() => setUserData(d)}
          >
            <DropdownToggle style={{ background: "#fff", border: "none" }}>
              <i
                className="ri-more-fill align-middle"
                style={{ color: "black" }}
              ></i>
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-end">
              <DropdownItem className="edit-item-btn d-flex align-items-center">
                <i className=" ri-eye-line  align-bottom me-2 text-muted"></i>
                View Product
              </DropdownItem>
              {/* <DropdownItem className="edit-item-btn d-flex align-items-center">
                <i className="  ri-edit-line align-bottom me-2 text-muted"></i>
                Edit Price
              </DropdownItem> */}
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>
      ),
    },
  ];
  const tableDataExtension = {
    columns: columns,
    data: [{}, {}, {}, {}, {}, {}, {}],
  };
  return (
    <div className="tab_price">
      <DataTableExtensions
        {...tableDataExtension}
        export={false}
        filterPlaceholder={`Search`}
        style={{ paddingRight: "25px important", display: "none" }}
      >
        <DataTable
          columns={columns}
          data={tableDataExtension}
          pagination
          paginationPerPage={5}
        />
      </DataTableExtensions>
    </div>
  );
};

export default ProductAppTable;
