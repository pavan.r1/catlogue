import React from "react";
import {
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Toast,
  ToastBody,
} from "reactstrap";

const AddproductMod = ({ open, setOpen, imageUrl }) => {
  const arr = [
    {
      desc: "watermark image",
    },
    { desc: "fake branded/1st copy" },
    { desc: "image with price" },
    { desc: "pixelated image" },
    { desc: "inverted image" },
    { desc: "blur/unclear image" },
    { desc: "incomplete image" },
    { desc: "stretched/shrunk image" },
    { desc: "image with props" },
    { desc: "image with text" },
  ];

  return (
    <div>
      <Modal
        id="signupModals"
        tabIndex="-1"
        isOpen={open}
        toggle={() => setOpen(false)}
        centered={true}
        size="lg"
      >
        <ModalHeader className="pt-2" toggle={() => setOpen(false)}>
          Products in a catalog
        </ModalHeader>

        <ModalBody>
          <div>
            <p
              className="m-0 mb-4 fs-14 text-muted"
              style={{ fontWeight: "500" }}
            >
              Please add only front image of your product.If you want to add
              multiple images for particular product,you can add it in next
              step.
            </p>
            <Row style={{ padding: 0, margin: 0 }}>
              <Col xs="6" style={{ padding: 0, margin: 0 }}>
                <div className="">
                  <Toast
                    isOpen={true}
                    id="borderedToast3"
                    className="bg-soft-warning overflow-hidden "
                    style={{ border: "1px solid #f6b94b" }}
                  >
                    <ToastBody>
                      <div className="d-flex align-items-center">
                        <div className="flex-shrink-0 me-2">
                          <i className=" ri-information-fill text-warning fs-21 align-middle"></i>
                        </div>
                        <div className="flex-grow-1">
                          <h6 className="mb-0">
                            You can add minimum 1 and maximum 9 products to
                            create a catalog
                          </h6>
                        </div>
                      </div>
                    </ToastBody>
                  </Toast>
                </div>
                <div className="mt-4 d-flex gap-3 align-items-center">
                  <img src={imageUrl} alt="" className="rounded avatar-sm" />
                  <div className="avatar-md">
                    <div
                      className="avatar-title rounded  text-primary d-flex flex-column align-items-center justify-content-center"
                      style={{
                        border: "1px dotted #d9d5fe",
                        background: "#f3f5f9",
                      }}
                    >
                      <i className=" ri-add-circle-line fs-16"></i>
                      <p className="m-0 fs-10">Add Products</p>
                    </div>
                  </div>
                </div>
              </Col>
              <Col xs="6">
                <p
                  className="m-0 mb-3 "
                  style={{ color: "#f90800", fontWeight: "500" }}
                >
                  🚫&nbsp;&nbsp;&nbsp; Images type which are not allowed
                </p>
                <Row>
                  {arr.map((item) => {
                    return (
                      <>
                        <Col
                          xs="6"
                          className="d-flex gap-2 align-items-center mb-3"
                        >
                          <img
                            src="https://images.pexels.com/photos/19090/pexels-photo.jpg?auto=compress&cs=tinysrgb&w=600"
                            alt=""
                            className="rounded avatar-sm"
                          />
                          <div>
                            <p className="m-0 fs-12 text-capitalize mb-1">
                              {item.desc}
                            </p>
                            <p
                              className="m-0 fa-13 text-muted"
                              style={{ fontWeight: "430" }}
                            >
                              🚫 NOT ALLOWED
                            </p>
                          </div>
                        </Col>
                      </>
                    );
                  })}
                </Row>
              </Col>
            </Row>
          </div>
        </ModalBody>
        <ModalFooter>
          <button
            type="button"
            className="btn btn-ghost-primary waves-effect waves-light me-3 "
            onClick={() => setOpen(false)}
          >
            Cancel
          </button>

          <button
            type="button"
            className="btn btn-primary waves-effect waves-light"
            style={{ fontWeight: "600" }}
          >
            continue
          </button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default AddproductMod;
