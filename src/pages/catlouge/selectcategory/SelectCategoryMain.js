import React, { useState } from "react";
import {
  Card,
  CardBody,
  Col,
  ListGroup,
  ListGroupItem,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
} from "reactstrap";
import { Nested1, Nested2, Nested3 } from "./Category1";
import "./category.css";
import CategoryDet from "./CategoryDet";

import classnames from "classnames";

const SelectCategoryMain = () => {
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);
  const [actlist, setActList] = useState(null);
  const [actlist2, setActList2] = useState(null);
  const [actlist3, setActList3] = useState(null);
  const [actlist4, setActList4] = useState(null);
  const [cardHeaderTab, setcardHeaderTab] = useState("1");
  const cardHeaderToggle = (tab) => {
    if (cardHeaderTab !== tab) {
      setcardHeaderTab(tab);
    }
  };

  const list1 = [
    "Cras justo odio",
    "Dapibus ac facilisis in",
    "Morbi leo risus",
    "Porta ac consectetur ac",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
    "Vestibulum at eros",
  ];
  return (
    <div className="page-content">
      <Row>
        <Col xxl={12}>
          {/* <h5 className="mb-3">Card Header Tabs</h5> */}
          <Card>
            <div>
              <p className="m-0 p-4 fs-18 fw-semi-bold">Add Single Catalouge</p>
            </div>
            <div className="card-header align-items-center d-flex">
              {/* <div className="flex-grow-1 oveflow-hidden">
                <p className="text-muted text-truncates mb-0">
                  Use <code>card-header-tabs</code> class to create card header
                  tabs.
                </p>
              </div> */}
              <div className="flex-shrink-0 ms-2">
                <Nav
                  tabs
                  className="nav justify-content-center nav-tabs-custom rounded card-header-tabs border-bottom-0"
                >
                  <NavItem>
                    <NavLink
                      style={{ cursor: "pointer" }}
                      className={classnames({ active: cardHeaderTab === "1" })}
                      onClick={() => {
                        cardHeaderToggle("1");
                      }}
                    >
                      Select Category
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      style={{ cursor: "pointer" }}
                      className={classnames({ active: cardHeaderTab === "2" })}
                      onClick={() => {
                        cardHeaderToggle("2");
                      }}
                    >
                      Add Product Details
                    </NavLink>
                  </NavItem>
                  {/* <NavItem>
                    <NavLink
                      style={{ cursor: "pointer" }}
                      className={classnames({ active: cardHeaderTab === "3" })}
                      onClick={() => {
                        cardHeaderToggle("3");
                      }}
                    >
                      Messages
                    </NavLink>
                  </NavItem> */}
                </Nav>
              </div>
            </div>
            {/* <CardBody></CardBody> */}
          </Card>
        </Col>
      </Row>

      <TabContent activeTab={cardHeaderTab} className="text-muted">
        <TabPane tabId="1" id="home2">
          <div style={{ width: "43%", marginBottom: "16px" }}>
            <label htmlFor="iconInput" className="form-label">
              Search Category
            </label>
            <div className="form-icon">
              <input
                type="email"
                className="form-control form-control-icon"
                id="iconInput"
                placeholder="Try sarees,Toys,Charger,Mugs and more"
              />
              <i className=" ri-search-line"></i>
            </div>
          </div>
          <Row>
            <Col xs="2">
              <div
                style={{
                  padding: "16px 11.2px",
                  background: "#ffffff",
                  border: "1px solid #f4f5f5",
                  fontSize: "14px",
                  color: "#000",
                }}
              >
                <i className=" ri-bookmark-line align-middle me-1"></i>
                Your Categories
              </div>
              {/* <hr style={{ height: "0.6px", margin: "0px" }} /> */}

              <ListGroup
                className="category-container"
                style={{
                  height: "426px",
                  overflowY: "scroll",
                  direction: "rtl",
                  textAlign: "left",
                  padding: "0px",
                  width: "173.6px",
                }}
              >
                {list1.map((item, index) => {
                  return (
                    <div key={index} className="d-flex flex-row-reverse">
                      <ListGroupItem
                        className={""}
                        style={{ flex: "1" }}
                        active={actlist == index ? true : false}
                        onClick={() => {
                          setShow1(true);
                          if (show2 === true || show3 === true) {
                            setShow2(false);
                            setShow3(false);
                          }
                          setActList(index);
                          setActList2(null);
                          setShow4(false);
                        }}
                      >
                        {item}
                        {/* <span className="arrow-div2"></span> */}
                      </ListGroupItem>
                    </div>
                  );
                })}
              </ListGroup>
              <div
                style={{
                  padding: "16px 11.2px",
                  background: "#ffffff",
                  border: "1px solid #f4f5f5",
                }}
              >
                <p className="m-0 fs-12">Can't find the category?</p>
                <p className="m-0 text-primary" style={{ fontWeight: "500" }}>
                  Search Category
                </p>
              </div>
            </Col>

            {show1 && (
              <Col xs="2">
                <Nested1
                  show={show2}
                  setShow={setShow2}
                  show3={show3}
                  setShow3={setShow3}
                  actlist={actlist2}
                  setActList={setActList2}
                  setActList3={setActList3}
                  setShow4={setShow4}
                />
              </Col>
            )}
            {show2 && (
              <Col xs="2">
                <Nested2
                  show={show3}
                  setShow={setShow3}
                  actlist={actlist3}
                  setActList={setActList3}
                  setShow4={setShow4}
                  show4={setShow4}
                  setActList4={setActList4}
                />
              </Col>
            )}
            {show3 && (
              <Col xs="2">
                <Nested3
                  show={show4}
                  setShow={setShow4}
                  actlist={actlist4}
                  setActList={setActList4}
                />
              </Col>
            )}
            <Col xs="4">{show4 && <CategoryDet />}</Col>
          </Row>
        </TabPane>

        <TabPane tabId="2" id="profile2">
          <p className="me-3 mb-0">
            Experiment and play around with the fonts that you already have in
            the software you’re working with reputable font websites. commodo
            enim craft beer mlkshk aliquip jean shorts ullamco ad vinyl cillum
            PBR. Homo nostrud organic, assumenda labore aesthetic magna
            delectus.commodo enim craft beer mlkshk aliquip jean shorts ullamco
            ad vinyl cillum PBR. Homo nostrud organic, assumenda labore
            aesthetic magna delectus Scale all elements of your design: text,
            elements, buttons, everything. Increase or decrease the letter
            spacing depending on the situation and try, try again until it looks
            right, and each /.
          </p>
        </TabPane>
      </TabContent>
    </div>
  );
};

export default SelectCategoryMain;
