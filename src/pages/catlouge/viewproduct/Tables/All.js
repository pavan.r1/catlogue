import React from "react";
import DataTable, { defaultThemes } from "react-data-table-component";
import DataTableExtensions from "react-data-table-component-extensions";
import img from "../../../../assets/images/small/img-9.jpg";
import "./all.css";

const customStyles = {
  //   header: {
  //     style: {
  //       minHeight: "56px",
  //     },
  //   },
  header: {
    style: {
      minHeight: "56px",
      backgroundColor: "blue", // Replace 'blue' with your desired background color
      color: "white", // Replace 'white' with your desired text color
    },
  },
  headRow: {
    style: {
      borderTopStyle: "solid",
      borderTopWidth: "1px",
      borderTopColor: defaultThemes.default.divider.default,
    },
  },
  headCells: {
    style: {
      backgroundColor: "#e6ebf2",
      "&:not(:last-of-type)": {
        borderRightStyle: "solid",
        borderRightWidth: "1px",
        borderRightColor: defaultThemes.default.divider.default,
      },
    },
  },
  cells: {
    style: {
      "&:not(:last-of-type)": {
        borderRightStyle: "solid",
        borderRightWidth: "1px",
        borderRightColor: defaultThemes.default.divider.default,
      },
    },
  },
};
const All = () => {
  const columns = [
    {
      name: "S.no",
      width: "60px",
      //   omit: true,
      selector: (row) => row.qc_admin_id,
      sortable: true,

      // width:'180px',
      //   center: true,
      cell: (d) => <div>1</div>,
    },

    {
      name: "Catalog Image",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,
      width: "160px",
      alignItems: "center",
      center: true,
      cell: (d) => (
        <div className="p-2">
          <img src={img} alt="img" width="80px" />
        </div>
      ),
    },
    {
      name: "Category",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>Gowns</div>,
    },
    {
      name: "File ID",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,
      width: "300px",
      alignItems: "center",
      center: true,
      cell: (d) => <div>12342343243-3432343-3243243-IN</div>,
    },
    {
      name: "Created Date",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,
      width: "200px",
      alignItems: "center",
      center: true,
      cell: (d) => <div>26June'23|05:29 PM</div>,
    },
    {
      name: "Products",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>1234</div>,
    },
    {
      name: "Qc Status",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => (
        <div>
          <span className="badge badge-soft-primary fs-12">
            <i className=" bx bx-save fs-12 align-middle me-1"></i>
            Draft
          </span>
        </div>
      ),
    },
    {
      name: "Action",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => (
        <div>
          <button
            type="button"
            className="btn btn-primary waves-effect waves-light"
            style={{ width: "100%" }}
          >
            Edit
          </button>
        </div>
      ),
    },

    // {
    //   name: "Details",
    //   //   width: "250px",
    //   selector: (row) => row.full_name,
    //   sortable: true,

    //   cell: (d) => (
    //     <div className="p-2">
    //       <div style={{ display: "flex", alignItems: "center", gap: "10px" }}>
    //         <div>
    //           {d?.brand_logo ? (
    //             <img
    //               src={"/user-dummy-img.jpg"}
    //               alt=""
    //               className="rounded-circle avatar-sm"
    //             />
    //           ) : (
    //             <div className="avatar-sm">
    //               <div className="avatar-title rounded-circle bg-soft-primary  text-primary">
    //                 {d?.brand_name?.charAt(0) ?? ""}
    //               </div>
    //             </div>
    //           )}
    //         </div>

    //         <div>
    //           <div className="fs-12">product name testing</div>
    //           <div className="fs-10 text-muted">
    //             {d?.cmp_name ? d?.cmp_name : d?.companyname ?? ""}
    //           </div>
    //           <div
    //             style={{ display: "flex", gap: "5px", alignItems: "center" }}
    //           ></div>
    //         </div>
    //       </div>
    //     </div>
    //   ),
    // },

    // {
    //   name: "Amount",
    //   selector: (row) => row.manager,
    //   sortable: true,
    //   //   omit: switchData ? omitData : !omitData,

    //   alignItems: "center",
    //   center: true,
    //   cell: (d) => <div>1234</div>,
    // },

    // {
    //   name: "Action",
    //   right: true,
    //   //   width: "70px",
    //   center: true,
    //   // omit:
    //   //   type == "fin" && (role == "manager" || role == "head") ? false : true,
    //   cell: (d) => (
    //     <div>
    //       <UncontrolledDropdown
    //         className="dropdown d-inline-block"
    //         // onClick={() => setUserData(d)}
    //       >
    //         <DropdownToggle style={{ background: "#fff", border: "none" }}>
    //           <i
    //             className="ri-more-fill align-middle"
    //             style={{ color: "black" }}
    //           ></i>
    //         </DropdownToggle>
    //         <DropdownMenu className="dropdown-menu-end">
    //           <DropdownItem
    //             className="edit-item-btn d-flex align-items-center"
    //             onClick={() => {
    //               history.push("/catlouge-viewproduct");
    //             }}
    //           >
    //             <i className=" ri-eye-line  align-bottom me-2 text-muted"></i>
    //             View Product
    //           </DropdownItem>
    //           <DropdownItem
    //             className="edit-item-btn d-flex align-items-center"
    //             onClick={() => {
    //               setOpen(!open);
    //             }}
    //           >
    //             <i className=" ri-eye-line  align-bottom me-2 text-muted"></i>
    //             View Product testing
    //           </DropdownItem>
    //           <DropdownItem className="edit-item-btn d-flex align-items-center">
    //             <i className="  ri-edit-line align-bottom me-2 text-muted"></i>
    //             Edit
    //           </DropdownItem>
    //         </DropdownMenu>
    //       </UncontrolledDropdown>
    //     </div>
    //   ),
    // },
  ];
  const tableDataExtension = {
    columns: columns,
    data: [{}, {}, {}, {}, {}, {}, {}],
  };
  return (
    <div className="all_table">
      <DataTableExtensions
        {...tableDataExtension}
        export={false}
        filterPlaceholder={`Search`}
        style={{ paddingRight: "", display: "none" }}
      >
        <DataTable
          columns={columns}
          data={tableDataExtension}
          pagination
          paginationPerPage={5}
          customStyles={customStyles}
        />
      </DataTableExtensions>
    </div>
  );
};

export default All;
