import React, { useRef, useState } from "react";
import {
  Accordion,
  AccordionItem,
  Card,
  CardBody,
  Col,
  Collapse,
  Row,
} from "reactstrap";
import BreadCrumb from "../../../components/common/BreadCrumb";
import classnames from "classnames";
import avatar from "../../../assets/images/users/avatar-1.jpg";
import "./viewproduct.css";
import ReactImageMagnify from "react-image-magnify";

const arrImg = [
  "https://m.media-amazon.com/images/I/91o0m2iIpVL._SL1500_.jpg",
  "https://m.media-amazon.com/images/I/91wCl-GH3eL._SL1500_.jpg",
  "https://m.media-amazon.com/images/I/9127mVKDZNL._SL1500_.jpg",
  "https://m.media-amazon.com/images/I/91vHGiybN7L._SL1500_.jpg",
  "https://m.media-amazon.com/images/I/81PfWIgv+PL._SL1500_.jpg",
  "https://m.media-amazon.com/images/I/A1ev2qXT8gL._SL1500_.jpg",
];

const ViewProduct = () => {
  const [plusiconCol1, setplusiconCol1] = useState(true);
  const [plusiconCol2, setplusiconCol2] = useState(false);
  const [plusiconCol3, setplusiconCol3] = useState(false);
  const [plusiconCol4, setplusiconCol4] = useState(false);
  const [plusiconCol5, setplusiconCol5] = useState(false);
  const [imgId, setImgId] = useState(
    "https://m.media-amazon.com/images/I/91o0m2iIpVL._SL1500_.jpg"
  );
  const scrollContainerRef = useRef(null);
  const hiddenUpload = useRef(null);

  console.log(imgId, "image");

  const t_plusiconCol1 = () => {
    setplusiconCol1(!plusiconCol1);
    setplusiconCol2(false);
    setplusiconCol3(false);
    setplusiconCol4(false);
    setplusiconCol5(false);
  };

  const t_plusiconCol2 = () => {
    setplusiconCol2(!plusiconCol2);
    setplusiconCol1(false);
    setplusiconCol3(false);
    setplusiconCol4(false);
    setplusiconCol5(false);
  };

  const t_plusiconCol4 = () => {
    setplusiconCol4(!plusiconCol4);
    setplusiconCol1(false);
    setplusiconCol2(false);
    setplusiconCol3(false);
    setplusiconCol5(false);
  };
  const t_plusiconCol5 = () => {
    setplusiconCol5(!plusiconCol5);
    setplusiconCol1(false);
    setplusiconCol2(false);
    setplusiconCol3(false);
    setplusiconCol4(false);
  };
  const t_plusiconCol3 = () => {
    setplusiconCol3(!plusiconCol3);
    setplusiconCol1(false);
    setplusiconCol2(false);
    setplusiconCol4(false);
    setplusiconCol5(false);
  };

  const handleScrollLeft = () => {
    if (scrollContainerRef.current) {
      scrollContainerRef.current.scrollTo({
        left: scrollContainerRef.current.scrollLeft - 100,
        behavior: "smooth", // Use 'auto' for instant scrolling
      });
    }
  };

  const handleScrollRight = () => {
    if (scrollContainerRef.current) {
      scrollContainerRef.current.scrollTo({
        left: scrollContainerRef.current.scrollLeft + 100,
        behavior: "smooth", // Use 'auto' for instant scrolling
      });
    }
  };

  const handleClick = () => {
    hiddenUpload.current.click();
  };

  const handleImageChange = (e) => {
    console.log(e.target.files[0]);
  };
  return (
    <div
      className={
        window.location.pathname === "/catlouge-viewproduct"
          ? "page-content"
          : ""
      }
    >
      {window.location.pathname === "/catlouge-viewproduct" ? (
        <BreadCrumb title="Products" pageTitle="Ecommerce" />
      ) : (
        <></>
      )}
      <Card>
        <CardBody>
          <Row>
            <Col xs="6" className="p-0">
              <div
                style={{
                  // padding: "12px",
                  display: "flex",
                  justifyContent: "center",
                  aspectRatio: " 16/9 ",
                }}
              >
                {/* <img src="/onion.jpg" alt="img" width="95%" height="px" /> */}
                <div>
                  <ReactImageMagnify
                    {...{
                      smallImage: {
                        alt: "Small Image",
                        src: imgId,
                        // src: "/onion.jpg",
                        width: 410,
                        height: 300,
                      },
                      largeImage: {
                        src: imgId,
                        width: 1200,
                        height: 900,
                      },
                      lensStyle: { backgroundColor: "rgba(0, 0, 0, 0.6)" },
                      enlargedImagePosition: "beside",
                      isActivatedOnTouch: true,
                    }}
                    style={{
                      zIndex: "99",
                      maxWidth: "100%",
                      maxHeight: "100%",
                      objectFit: "contain",
                    }}
                  />
                </div>
              </div>
              <div style={{ padding: "12px", display: "flex" }}>
                <div
                  className="d-flex gap-2   "
                  ref={scrollContainerRef}
                  style={{
                    whiteSpace: "nowrap",
                    width: "488px",
                    overflowX: "scroll",
                  }}
                >
                  <div
                    style={{
                      position: "sticky",
                      top: "0px",
                      left: "0px",
                      padding: "8px",
                      background: "#fff",
                    }}
                    // onClick={handleScrollLeft}
                  >
                    <span
                      className="cursor-pointer"
                      style={{
                        position: "sticky",
                        top: "0px",
                        left: "0px",
                      }}
                      onClick={handleScrollLeft}
                    >
                      <i
                        className=" ri-arrow-left-s-line fs-21 bg-light text-primary py-1 px-1"
                        style={{ borderRadius: "8px" }}
                      ></i>
                    </span>
                  </div>
                  {arrImg.map((item) => {
                    return (
                      <>
                        <div
                          className="img-zoom cursor-pointer "
                          style={{
                            borderColor: imgId === item ? "#85c225" : null,
                          }}
                        >
                          <img
                            src={item}
                            alt="img"
                            width={"50px"}
                            onClick={() => setImgId(item)}
                          ></img>
                        </div>
                      </>
                    );
                  })}
                  <div className="d-flex align-items-center cursor-pointer ">
                    <i
                      className=" ri-add-circle-line fs-24 text-primary"
                      onClick={handleClick}
                    ></i>
                    <input
                      className="d-none"
                      type="file"
                      ref={hiddenUpload}
                      onChange={handleImageChange}
                    />
                  </div>

                  <div
                    style={{
                      position: "absolute",
                      top:
                        window.location.pathname === "/catlouge-viewproduct"
                          ? "334.5px"
                          : "316.5px",
                      right:
                        window.location.pathname === "/catlouge-viewproduct"
                          ? "31px"
                          : "3px",
                      // left: "0px",
                      padding: "12px 8px",
                      background: "#fff",
                    }}
                  >
                    <span
                      className="cursor-pointer"
                      // style={{
                      //   position: "absolute",
                      //   top: "0px",
                      //   right: "0px",
                      //   left: "0px",
                      // }}
                      onClick={handleScrollRight}
                    >
                      <i
                        className=" ri-arrow-right-s-line fs-21 bg-light px-1 py-1 text-primary"
                        style={{ borderRadius: "8px" }}
                      ></i>
                    </span>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="6" className="p-0">
              <div
              // style={{ padding: "12px" }}
              >
                <p style={{ textDecoration: "underline", fontSize: "14px" }}>
                  Sub category list
                </p>
                <p style={{ fontSize: "20px", fontWeight: "500" }}>
                  Aashirvaad Atta/Godihittu - Whole Wheat, 5 kg
                </p>
                <p style={{ textDecoration: "line-through", color: "#929292" }}>
                  MRP:₹123
                </p>
                <p style={{ fontSize: "15px", fontWeight: "600" }}>
                  Price:₹100
                </p>
                <p
                  style={{
                    color: "#ba5252",
                    fontSize: "12px",
                    fontWeight: "500",
                  }}
                >
                  You Save:23
                </p>
              </div>
            </Col>
            <Col xs="12" className="mt-4">
              <h4 className="mb-4" style={{ textDecoration: "underline" }}>
                Aashirvaad Atta/Godihittu - Whole Wheat
              </h4>
              <Accordion
                className="custom-accordionwithicon-plus"
                id="accordionWithplusicon"
              >
                <AccordionItem>
                  <h2
                    className="accordion-header"
                    id="accordionwithouticonExample1"
                  >
                    <button
                      className={classnames("accordion-button", {
                        collapsed: !plusiconCol1,
                      })}
                      type="button"
                      onClick={t_plusiconCol1}
                      style={{ cursor: "pointer" }}
                    >
                      Product Info
                    </button>
                  </h2>

                  <Collapse
                    isOpen={plusiconCol1}
                    className="accordion-collapse"
                    id="accor_plusExamplecollapse1"
                  >
                    <div className="accordion-body">
                      Big July earthquakes confound zany experimental vow. My
                      girl wove six dozen plaid jackets before she quit. Six big
                      devils from Japan quickly forgot how to waltz. try again
                      until it looks right, and each assumenda labore aes Homo
                      nostrud organic, assumenda labore aesthetic magna
                      elements, buttons, everything.
                    </div>
                  </Collapse>
                </AccordionItem>
                <AccordionItem>
                  <h2
                    className="accordion-header"
                    id="accordionwithplusExample2"
                  >
                    <button
                      className={classnames("accordion-button", {
                        collapsed: !plusiconCol2,
                      })}
                      type="button"
                      onClick={t_plusiconCol2}
                      style={{ cursor: "pointer" }}
                    >
                      Company Info
                    </button>
                  </h2>

                  <Collapse
                    isOpen={plusiconCol2}
                    className="accordion-collapse"
                    id="accor_plusExamplecollapse2"
                  >
                    <div className="accordion-body">
                      It makes a statement, it’s impressive graphic design.
                      Increase or decrease the letter spacing depending on the
                      situation and try, try again until it looks right, and
                      each letter has the perfect spot of its own. commodo enim
                      craft beer mlkshk aliquip jean shorts ullamco.
                    </div>
                  </Collapse>
                </AccordionItem>
                <AccordionItem>
                  <h2
                    className="accordion-header"
                    id="accordionwithplusExample3"
                  >
                    <button
                      className={classnames("accordion-button", {
                        collapsed: !plusiconCol3,
                      })}
                      type="button"
                      onClick={t_plusiconCol3}
                      style={{ cursor: "pointer" }}
                    >
                      Regulatory Info
                    </button>
                  </h2>
                  <Collapse
                    isOpen={plusiconCol3}
                    className="accordion-collapse"
                    id="accor_plusExamplecollapse3"
                  >
                    <div className="accordion-body">
                      Spacing depending on the situation and try, try again
                      until it looks right, and each. next level wes anderson
                      artisan four loko farm-to-table craft beer twee. commodo
                      enim craft beer mlkshk aliquip jean shorts ullamco. omo
                      nostrud organic, assumenda labore aesthetic magna
                      delectus. pposites attract, and that’s a fact.
                    </div>
                  </Collapse>
                </AccordionItem>
                <AccordionItem>
                  <h2
                    className="accordion-header"
                    id="accordionwithplusExample3"
                  >
                    <button
                      className={classnames("accordion-button", {
                        collapsed: !plusiconCol4,
                      })}
                      type="button"
                      onClick={t_plusiconCol4}
                      style={{ cursor: "pointer" }}
                    >
                      Customer Care Info
                    </button>
                  </h2>
                  <Collapse
                    isOpen={plusiconCol4}
                    className="accordion-collapse"
                    id="accor_plusExamplecollapse3"
                  >
                    <div className="accordion-body">
                      Spacing depending on the situation and try, try again
                      until it looks right, and each. next level wes anderson
                      artisan four loko farm-to-table craft beer twee. commodo
                      enim craft beer mlkshk aliquip jean shorts ullamco. omo
                      nostrud organic, assumenda labore aesthetic magna
                      delectus. pposites attract, and that’s a fact.
                    </div>
                  </Collapse>
                </AccordionItem>
                <AccordionItem>
                  <h2
                    className="accordion-header"
                    id="accordionwithplusExample3"
                  >
                    <button
                      className={classnames("accordion-button", {
                        collapsed: !plusiconCol5,
                      })}
                      type="button"
                      onClick={t_plusiconCol5}
                      style={{ cursor: "pointer" }}
                    >
                      Other Info
                    </button>
                  </h2>
                  <Collapse
                    isOpen={plusiconCol5}
                    className="accordion-collapse"
                    id="accor_plusExamplecollapse3"
                  >
                    <div className="accordion-body">
                      Spacing depending on the situation and try, try again
                      until it looks right, and each. next level wes anderson
                      artisan four loko farm-to-table craft beer twee. commodo
                      enim craft beer mlkshk aliquip jean shorts ullamco. omo
                      nostrud organic, assumenda labore aesthetic magna
                      delectus. pposites attract, and that’s a fact.
                    </div>
                  </Collapse>
                </AccordionItem>
              </Accordion>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  );
};

export default ViewProduct;
