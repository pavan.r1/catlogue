import React from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";
import ViewProduct from "./ViewProduct";

const ViewProductModal = ({ open, setOpen }) => {
  return (
    <div>
      <Modal
        id="signupModals"
        tabIndex="-1"
        isOpen={open}
        toggle={() => setOpen(false)}
        centered={true}
        size="xl"
      >
        <ModalHeader
          className="p-3"
          toggle={() => setOpen(false)}
        ></ModalHeader>

        <ModalBody>
          <ViewProduct />
        </ModalBody>
      </Modal>
    </div>
  );
};

export default ViewProductModal;
