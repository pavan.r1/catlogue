import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import classnames from "classnames";
import BreadCrumb from "../../../../components/common/BreadCrumb";
import General from "./General";
import OptVar from "./OptVar";
import Misc from "./Misc";

const AddProductMain = () => {
  const [customActiveTab, setcustomActiveTab] = useState("1");
  const toggleCustom = (tab) => {
    if (customActiveTab !== tab) {
      setcustomActiveTab(tab);
    }
  };
  return (
    <div className="page-content">
      <Col xxl={12}>
        <BreadCrumb title="Add Products" pageTitle="Ecommerce" />
        <Card>
          <CardBody>
            <Nav
              tabs
              className="nav nav-tabs nav-tabs-custom nav-primary nav-justified mb-3"
            >
              <NavItem>
                <NavLink
                  style={{ cursor: "pointer" }}
                  className={classnames({
                    active: customActiveTab === "1",
                  })}
                  onClick={() => {
                    toggleCustom("1");
                  }}
                >
                  General
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  style={{ cursor: "pointer" }}
                  className={classnames({
                    active: customActiveTab === "2",
                  })}
                  onClick={() => {
                    toggleCustom("2");
                  }}
                >
                  Options & Variants
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  style={{ cursor: "pointer" }}
                  className={classnames({
                    active: customActiveTab === "3",
                  })}
                  onClick={() => {
                    toggleCustom("3");
                  }}
                >
                  Miscellaneous
                </NavLink>
              </NavItem>
              {/* <NavItem>
                <NavLink
                  style={{ cursor: "pointer" }}
                  className={classnames({
                    active: customActiveTab === "4",
                  })}
                  onClick={() => {
                    toggleCustom("4");
                  }}
                >
                  Settings
                </NavLink>
              </NavItem> */}
            </Nav>

            <TabContent activeTab={customActiveTab} className="text-muted">
              <TabPane tabId="1" id="home1">
                <General />
              </TabPane>
              <TabPane tabId="2">
                <OptVar />
              </TabPane>
              <TabPane tabId="3">
                <Misc />
              </TabPane>
              <TabPane tabId="4">
                <div className="d-flex">
                  <div className="flex-shrink-0">
                    <i className="ri-checkbox-multiple-blank-fill text-success"></i>
                  </div>
                  <div className="flex-grow-1 ms-2">
                    when darkness overspreads my eyes, and heaven and earth seem
                    to dwell in my soul and absorb its power, like the form of a
                    beloved mistress, then I often think with longing, Oh, would
                    I could describe these conceptions, could impress upon paper
                    all that is living so full and warm within me, that it might
                    be the.
                    <div className="mt-2">
                      <Link to="#" className="btn btn-sm btn-soft-primary">
                        Read More{" "}
                        <i className="ri-arrow-right-line ms-1 align-middle"></i>
                      </Link>
                    </div>
                  </div>
                </div>
              </TabPane>
            </TabContent>
          </CardBody>
        </Card>
      </Col>
    </div>
  );
};

export default AddProductMain;
