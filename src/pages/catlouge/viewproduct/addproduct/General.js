import React, { useState } from "react";
import { Col, Row } from "reactstrap";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
// Import React FilePond
import { FilePond, registerPlugin } from "react-filepond";
// Import FilePond styles
import "filepond/dist/filepond.min.css";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import "./general.css";

// Register the plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

const General = () => {
  const [files, setFiles] = useState([]);
  const [selectedOption, setSelectedOption] = useState("manualupload");

  const handleOptionChange = (event) => {
    setSelectedOption(event.target.value);
    console.log(event.target.value);
  };
  return (
    <div>
      <div>
        <Row className="align-items-center ">
          <Col xs="2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Name
            </p>
          </Col>
          <Col xs="10">
            <input
              type="text"
              className="form-control"
              id="basiInput"
              style={{
                border: "none",
                borderBottom: "1px solid #ebebeb",
                borderRadius: "0px",
              }}
            />
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Alias
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <input
              type="text"
              className="form-control"
              id="basiInput"
              style={{
                border: "none",
                borderBottom: "1px solid #ebebeb",
                borderRadius: "0px",
              }}
            />
          </Col>
          <Row className="mt-2">
            <Col xs="2" className="mt-2">
              <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
                Description
              </p>
            </Col>
            <Col xs="10" className="mt-2">
              <CKEditor
                editor={ClassicEditor}
                // data={sowscript}
                className="editor"
                // onReady={(editor) => {
                //   editor.editing.view.change((writer) => {
                //     writer.setStyle(
                //       "height",
                //       "20px",
                //       editor.editing.view.document.getRoot()
                //     );
                //   });
                // }}
                // onChange={(event, editor) => {
                //   const edit = editor.getData();
                //   console.log(edit, "123456");
                //   setEditScript(edit);
                //   setEdited(true);
                // }}
              />
            </Col>
          </Row>
          <Row className="mt-2">
            <Col xs="2" className="mt-2">
              <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
                Images
              </p>
            </Col>
            <Col xs="10" className="mt-2">
              <FilePond
                files={files}
                onupdatefiles={setFiles}
                allowMultiple={true}
                maxFiles={3}
                name="files"
                className="filepond filepond-input-multiple"
              />
            </Col>
          </Row>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Price
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <div className="d-flex">
              <span
                className="align-self-center mt-3 pb-1"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              >
                ₹
              </span>
              <input
                type="text"
                className="form-control"
                id="basiInput"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              />
            </div>
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Maximum Retail Price(MRP)
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <div className="d-flex">
              <span
                className="align-self-center mt-3 pb-1"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              >
                ₹
              </span>
              <input
                type="text"
                className="form-control"
                id="basiInput"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              />
            </div>
          </Col>
          <Col xs="2" className="mt-4">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Inventory Management
            </p>
          </Col>
          <Col xs="10" className="mt-4">
            <div className="d-flex gap-5 ">
              <div className="d-flex gap-3 align-items-center">
                <input
                  type="radio"
                  value="manualupload"
                  checked={selectedOption === "manualupload"}
                  style={{ transform: "scale(1.3)" }}
                  onChange={handleOptionChange}
                />
                <label style={{ margin: "0px", fontSize: "12px" }}>
                  Don't track my inventory
                </label>
              </div>
              <div className="d-flex gap-3 align-items-center">
                <input
                  type="radio"
                  value="bulkupload"
                  checked={selectedOption === "bulkupload"}
                  style={{ transform: "scale(1.3)" }}
                  onChange={handleOptionChange}
                />
                <label style={{ margin: "0px", fontSize: "12px" }}>
                  Track My inventory
                </label>
              </div>
            </div>
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              HSN/SAC
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <input
              type="text"
              className="form-control"
              id="basiInput"
              style={{
                border: "none",
                borderBottom: "1px solid #ebebeb",
                borderRadius: "0px",
              }}
            />
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Product Weight
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <div className="d-flex">
              <span
                className="align-self-center mt-3 pb-1"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              >
                gm
              </span>
              <input
                type="text"
                className="form-control"
                id="basiInput"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              />
            </div>
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Package Weight
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <div className="d-flex">
              <span
                className="align-self-center mt-3 pb-1"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              >
                gm
              </span>
              <input
                type="text"
                className="form-control"
                id="basiInput"
                style={{
                  border: "none",
                  borderBottom: "1px solid #ebebeb",
                  borderRadius: "0px",
                }}
              />
            </div>
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Tax
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <input
              type="text"
              className="form-control"
              id="basiInput"
              style={{
                border: "none",
                borderBottom: "1px solid #ebebeb",
                borderRadius: "0px",
              }}
            />
          </Col>
          <Col xs="2" className="mt-2">
            <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
              Publish
            </p>
          </Col>
          <Col xs="10" className="mt-2">
            <input
              type="text"
              className="form-control"
              id="basiInput"
              style={{
                border: "none",
                borderBottom: "1px solid #ebebeb",
                borderRadius: "0px",
              }}
            />
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default General;
