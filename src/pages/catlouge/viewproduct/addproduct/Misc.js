import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  UncontrolledDropdown,
} from "reactstrap";

const Misc = () => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [nestedDropdownOpen, setNestedDropdownOpen] = useState(false);
  const [showNestedDropdown, setShowNestedDropdown] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  const handleMouseEnter = () => {
    setIsOpen(true);
  };

  const handleMouseLeave = () => {
    setIsOpen(false);
  };

  const toggleNestedDropdown = () => {
    setShowNestedDropdown(!showNestedDropdown);
  };

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  return (
    <div>
      <div>
        <UncontrolledDropdown
          className="me-2"
          direction="down"
          isOpen={showNestedDropdown}
          toggle={toggleNestedDropdown}
        >
          <DropdownToggle caret color="primary">
            Dropdown
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>
              <UncontrolledDropdown
                onClick={handleMouseEnter}
                // onMouseEnter={handleMouseEnter}
                // onMouseLeave={handleMouseLeave}
                isOpen={isOpen}
                direction="end"
              >
                <DropdownToggle tag="span" caret={false}>
                  Hover Dropdown
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={handleMouseLeave}>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>Option 2</DropdownItem>
                  <DropdownItem>Option 3</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </DropdownItem>
            <DropdownItem>List Item 2</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>List Item 3</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>

        {showNestedDropdown && (
          <UncontrolledDropdown className="me-2" direction="end">
            <DropdownToggle caret color="primary">
              Dropdown
            </DropdownToggle>
            <DropdownMenu>
              <DropdownItem>Nested Item 1</DropdownItem>
              <DropdownItem>Nested Item 2</DropdownItem>
              <DropdownItem divider />
              <DropdownItem>Nested Item 3</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        )}
      </div>
    </div>
  );
};

export default Misc;
