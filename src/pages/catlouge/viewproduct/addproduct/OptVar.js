import React from "react";
import { Card, CardBody, CardColumns, Col, Row } from "reactstrap";

const OptVar = () => {
  return (
    <div>
      <Row>
        <Col xs="2">
          <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
            Options
          </p>
        </Col>
        <Col xs="10">
          <Card
            style={{
              boxShadow:
                " rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px ",
            }}
          >
            <CardBody>
              <Row>
                <Col xs="4">
                  <input
                    type="text"
                    className="form-control"
                    id="basiInput"
                    placeholder="Name"
                    style={{
                      border: "none",
                      borderBottom: "1px solid #ebebeb",
                      borderRadius: "0px",
                      marginBottom: "4px",
                    }}
                  />
                  <span className="fs-11  text-dark">
                    E.g. size,colour(use small case)
                  </span>
                </Col>
                <Col xs="8">
                  <input
                    type="text"
                    className="form-control"
                    id="basiInput"
                    placeholder="Value"
                    style={{
                      border: "none",
                      borderBottom: "1px solid #ebebeb",
                      borderRadius: "0px",
                      marginBottom: "4px",
                    }}
                  />
                  <span className="fs-11  text-dark">
                    E.g. small,medium,large
                  </span>
                </Col>
              </Row>
              <Row className="mt-4 align-items-center">
                <Col xs="7">
                  <button
                    type="button"
                    className="btn btn-primary waves-effect waves-light btn-sm"
                  >
                    Add Options
                  </button>
                </Col>
                <Col xs="5">
                  <select
                    className="form-select "
                    aria-label="Default select example"
                    style={{
                      width: "100%",
                      border: "none",
                      borderBottom: "1px solid #ebebeb",
                      borderRadius: "0px",
                      // marginBottom: "4px",
                    }}
                  >
                    <option selected>Option set</option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col xs="2">
          <p className="m-0 text-dark" style={{ fontWeight: "500" }}>
            Variants
          </p>
        </Col>
        <Col xs="10">
          <Card
            style={{
              boxShadow:
                " rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px ",
            }}
          >
            <div
              style={{
                background: "#e6e6e6",
                textAlign: "center",
                padding: "6px",
              }}
              className="text-danger"
            >
              Please add options to generate variants !
            </div>
            <CardBody>
              <p className="m-o fw-bold text-dark text-center">
                No Records Found
              </p>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default OptVar;
