import React from "react";
import { Link } from "react-router-dom";
import { Card, CardBody, Col, Row } from "reactstrap";
import company1 from "../../assets/images/companies/img-1.png";
import company2 from "../../assets/images/companies/img-2.png";
import company3 from "../../assets/images/companies/img-3.png";
import company4 from "../../assets/images/companies/img-4.png";
import company5 from "../../assets/images/companies/img-5.png";
import company6 from "../../assets/images/companies/img-6.png";
import company7 from "../../assets/images/companies/img-7.png";
import company8 from "../../assets/images/companies/img-8.png";
import SellerChats from "./SellerChats";

const sellersList = [
  {
    id: 1,
    isTrending: true,
    img: company1,
    label: "Force Medicines",
    name: "David Marshall",
    stock: "452",
    chartdata: [12, 14, 2, 47, 42, 15, 47, 75, 65, 19, 14],
    color: "#f06548",
    balance: "$45,415",
  },
  {
    id: 2,
    isTrending: false,
    img: company2,
    label: "Micro Design",
    name: "Katia Stapleton",
    stock: "784",
    chartdata: [12, 14, 2, 47, 42, 15, 35, 75, 20, 67, 89],
    color: "#0ab39c",
    balance: "$97,642",
  },
  {
    id: 3,
    isTrending: false,
    img: company3,
    label: "Nesta Technologies",
    name: "Harley Fuller",
    stock: "320",
    chartdata: [45, 20, 8, 42, 30, 5, 35, 79, 22, 54, 64],
    color: "#f7b84b",
    balance: "$27,102",
  },
  {
    id: 4,
    isTrending: true,
    img: company4,
    label: "iTest Factory",
    name: "Oliver Tyler",
    stock: "159",
    chartdata: [26, 15, 48, 12, 47, 19, 35, 19, 85, 68, 50],
    color: "#0ab39c",
    balance: "$14,933",
  },
  {
    id: 5,
    isTrending: false,
    img: company5,
    label: "Meta4Systems",
    name: "Zoe Dennis",
    stock: "363",
    chartdata: [60, 67, 12, 49, 6, 78, 63, 51, 33, 8, 16],
    color: "#f7b84b",
    balance: "$73,426",
  },
  {
    id: 6,
    isTrending: true,
    img: company6,
    label: "Digitech Galaxy",
    name: "John Roberts",
    stock: "412",
    chartdata: [78, 63, 51, 33, 8, 16, 60, 67, 12, 49],
    color: "#0ab39c",
    balance: "$34,241",
  },
  {
    id: 7,
    isTrending: true,
    img: company7,
    label: "Syntyce Solutions",
    name: "Demi Allen",
    stock: "945",
    chartdata: [15, 35, 75, 20, 67, 8, 42, 30, 5, 35],
    color: "#f06548",
    balance: "$17,200",
  },
  {
    id: 8,
    isTrending: false,
    img: company8,
    label: "Zoetic Fashion",
    name: "James Bowen",
    stock: "784",
    chartdata: [45, 32, 68, 55, 36, 10, 48, 25, 74, 54],
    color: "#f7b84b",
    balance: "$97,642",
  },
];

const ProductList = () => {
  return (
    <div className="page-content">
      <Row className="mt-4">
        {sellersList.map((seller, key) => (
          <React.Fragment key={key}>
            <Col xl={3} lg={6}>
              <Card className="ribbon-box right overflow-hidden">
                <CardBody className="text-center p-4">
                  {seller.isTrending && (
                    <div className="ribbon ribbon-info ribbon-shape trending-ribbon">
                      <i className="ri-flashlight-fill text-white align-bottom"></i>{" "}
                      <span className="trending-ribbon-text">Trending</span>
                    </div>
                  )}
                  <img src={seller.img} alt="" height="45" />
                  <h5 className="mb-1 mt-4">
                    <Link
                      to="apps-ecommerce-seller-details"
                      className="link-primary"
                    >
                      {seller.label}
                    </Link>
                  </h5>
                  <p className="text-muted mb-4">{seller.name}</p>
                  <Row className="justify-content-center">
                    <Col lg={8}>
                      <SellerChats
                        color={seller.color}
                        data={seller.chartdata}
                      />
                    </Col>
                  </Row>
                  <Row className="mt-4">
                    <Col lg={6} className="border-end-dashed border-end">
                      <h5>{seller.stock}</h5>
                      <span className="text-muted">Item Stock</span>
                    </Col>
                    <Col lg={6}>
                      <h5>{seller.balance}</h5>
                      <span className="text-muted">Wallet Balance</span>
                    </Col>
                  </Row>
                  <div className="mt-4">
                    <Link to="/seller" className="btn btn-light w-100">
                      View Details
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </React.Fragment>
        ))}
      </Row>
    </div>
  );
};

export default ProductList;
