import React from "react";
import { Link } from "react-router-dom";
import { Card, CardBody, Col, Row, Table } from "reactstrap";
import avatar from "../../assets/images/users/avatar-1.jpg";

const SellerSideCard = () => {
  return (
    <div>
      <Card>
        <CardBody className="p-4">
          <div>
            <div className="flex-shrink-0 avatar-md mx-auto">
              <div className="avatar-title bg-light rounded">
                <img src={avatar} alt="" height="50" />
              </div>
            </div>
            <div className="mt-4 text-center">
              <h5 className="mb-1">Force Medicines</h5>
              <p className="text-muted">Since 1987</p>
            </div>
            <div className="table-responsive">
              <Table className="table mb-0 table-borderless">
                <tbody>
                  <tr>
                    <th>
                      <span className="fw-medium">Owner Name</span>
                    </th>
                    <td>David Marshall</td>
                  </tr>
                  <tr>
                    <th>
                      <span className="fw-medium">Company Type</span>
                    </th>
                    <td>Partnership</td>
                  </tr>
                  <tr>
                    <th>
                      <span className="fw-medium">Email</span>
                    </th>
                    <td>forcemedicines@gamil.com</td>
                  </tr>
                  <tr>
                    <th>
                      <span className="fw-medium">Website</span>
                    </th>
                    <td>
                      <Link to="#" className="link-primary">
                        www.forcemedicines.com
                      </Link>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <span className="fw-medium">Contact No.</span>
                    </th>
                    <td>+(123) 9876 654 321</td>
                  </tr>
                  <tr>
                    <th>
                      <span className="fw-medium">Fax</span>
                    </th>
                    <td>+1 999 876 5432</td>
                  </tr>
                  <tr>
                    <th>
                      <span className="fw-medium">Location</span>
                    </th>
                    <td>United Kingdom</td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};

export default SellerSideCard;
