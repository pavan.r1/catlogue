import React from "react";
import {
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap";
import DataTable from "react-data-table-component";
import DataTableExtensions from "react-data-table-component-extensions";
import "./sellertable.css";
import product from "../../assets/images/products/img-1.png";
const SellerTable = () => {
  const columns = [
    {
      name: "ID",
      width: "100px",
      omit: true,
      selector: (row) => row.qc_admin_id,
      sortable: true,

      // width:'180px',
      center: true,
      cell: (d) => <div>kml</div>,
    },

    {
      name: "Details",
      selector: (row) => row.full_name,
      sortable: true,
      width: "350px",
      // center: true,

      cell: (d) => (
        <div className="p-2">
          {/* <div style={{ display: "flex", alignItems: "center", gap: "10px" }}>
            <div>
              {d?.brand_logo ? (
                <img
                  src={"/user-dummy-img.jpg"}
                  alt=""
                  className="rounded-circle avatar-sm"
                />
              ) : (
                <div className="avatar-sm">
                  <div className="avatar-title rounded-circle bg-soft-primary  text-primary">
                    {d?.brand_name?.charAt(0) ?? ""}
                  </div>
                </div>
              )}
            </div>

            <div>
              <div className="fs-12">product name testing</div>
              <div className="fs-10 text-muted">
                {d?.cmp_name ? d?.cmp_name : d?.companyname ?? ""}
              </div>
              <div
                style={{ display: "flex", gap: "5px", alignItems: "center" }}
              ></div>
            </div>
          </div> */}
          <>
            <div className="d-flex align-items-center">
              <div className="flex-shrink-0 me-3">
                <div className="avatar-sm bg-light rounded p-1">
                  <img src={product} alt="" className="img-fluid d-block" />
                </div>
              </div>
              <div className="flex-grow-1">
                <h5 className="fs-14 mb-1">
                  <a
                    href="apps-ecommerce-product-details"
                    className="text-dark"
                  >
                    Half Sleeve Round Neck T-Shirts
                  </a>
                </h5>
                <p className="text-muted mb-0">
                  Category : <span className="fw-medium">Clothes</span>
                </p>
              </div>
            </div>
          </>
        </div>
      ),
    },

    {
      name: "Stock",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>1234</div>,
    },
    {
      name: "Price",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>₹1234</div>,
    },
    {
      name: "Orders",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>1234</div>,
    },
    {
      name: "Rating",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => (
        <div>
          <span>
            <span className="badge bg-light text-body fs-12 fw-medium">
              <i className="mdi mdi-star text-warning me-1"></i>
              {4.2}
            </span>
          </span>
        </div>
      ),
    },
    {
      name: "Published",
      selector: (row) => row.manager,
      sortable: true,
      //   omit: switchData ? omitData : !omitData,

      alignItems: "center",
      center: true,
      cell: (d) => <div>12 Oct,2021</div>,
    },
    {
      name: "Action",
      right: true,
      //   width: "70px",
      center: true,
      // omit:
      //   type == "fin" && (role == "manager" || role == "head") ? false : true,
      cell: (d) => (
        <div>
          <UncontrolledDropdown
            className="dropdown d-inline-block"
            // onClick={() => setUserData(d)}
          >
            <DropdownToggle style={{ background: "#fff", border: "none" }}>
              <i
                className="ri-more-fill align-middle"
                style={{ color: "black" }}
              ></i>
            </DropdownToggle>
            <DropdownMenu className="dropdown-menu-end">
              <DropdownItem
                className="edit-item-btn d-flex align-items-center"
                onClick={() => {
                  //   history.push("/catlouge-viewproduct");
                  console.log("hii");
                }}
              >
                <i className=" ri-eye-line  align-bottom me-2 text-muted"></i>
                View Product
              </DropdownItem>
              <DropdownItem className="edit-item-btn d-flex align-items-center">
                <i className="  ri-edit-line align-bottom me-2 text-muted"></i>
                Edit
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div>
      ),
    },
  ];
  const tableDataExtension = {
    columns: columns,
    data: [{}, {}, {}, {}, {}, {}, {}],
  };
  return (
    <div className="seller-table">
      <DataTableExtensions
        {...tableDataExtension}
        export={false}
        filterPlaceholder={`Search`}
        style={{ paddingRight: "25px important", display: "none" }}
      >
        <DataTable
          columns={columns}
          data={tableDataExtension}
          pagination
          paginationPerPage={5}
          selectableRows
          onSelectedRowsChange={(d) => console.log(d, "edd")}
        />
      </DataTableExtensions>
    </div>
  );
};

export default SellerTable;
