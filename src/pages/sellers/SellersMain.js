import React from "react";
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import SellerGraph from "./SellerGraph";
import SellerSideCard from "./SellerSideCard";
import SellerTable from "./SellerTable";

const SellersMain = () => {
  return (
    <div className="page-content">
      <Row>
        <Col xs="3">
          <SellerSideCard />
        </Col>
        <Col xs="9">
          <SellerGraph />
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5>Table</h5>
                <div style={{ marginRight: "228px" }}>
                  <button
                    type="button"
                    className="btn btn-success waves-effect waves-light"
                  >
                    Add New
                  </button>
                </div>
              </div>
            </CardHeader>
            <CardBody>
              <SellerTable />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default SellersMain;
